
class Config(object):
    """
    Common configurations
    """

    # Put here any configuration common to all environements
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class DevelopmentsConfig(Config):
    """
    Development Configurations
    """

    # logs errors
    SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    """
    Production Configurations
    """

    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    """
    Testing Configuration
    """

    TESTING = True

app_config = {
    'development': DevelopmentsConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
