
from app import db, login_manager

from flask_login import UserMixin

from werkzeug.security import check_password_hash, generate_password_hash


# TODO: try on Model per file.
class Employee(UserMixin, db.Model):
    """
    Create an Employee table
    """

    # Ensures the table will be names in plural and not in singular, once the Model name is in singular.
    __tablename__ = 'employees'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    password_hash = db.Column(db.String(128))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    is_admin = db.Column(db.Boolean, default=False)

    # TODO: check if in Python it can be set to private to prevent access.
    @property
    def password(self):
        """
        Prevent password from being accessed
        """

        raise AttributeError('password is not a readable attribute.')

    # TODO: see if is possible to not use setters in python.
    @password.setter
    def password(self, password):
        """"
        Set password to be a hashed password
        """

        self.password_hash = generate_password_hash(password)

    # TODO: try to use Repository pattern for this.
    def verify_password(self, password):
        """
        Check if hashed password matches actual password.
        """

        return check_password_hash(self.password_hash, password)

    # TODO: learn about __repr__
    def __repr__(self):
        return '<Employee: {}>'.format(self.username)


# Set up user_loader
@login_manager.user_loader
# TODO: a function outside the Model class???
def load_user(user_id):
    return Employee.query.get(int(user_id))


class Department(db.Model):
    """
    Create a Department table
    """

    __tablename__ = 'departments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    employees = db.relationship('Employee', backref='department', lazy='dynamic')

    def __repr__(self):
        return '<Department: {}>'.format(self.name)


class Role(db.Model):
    """
    Create a Role table
    """

    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    employees = db.relationship('Employee', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role: {}>'.format(self.name)
