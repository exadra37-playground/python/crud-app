from config import app_config

from flask import Flask, render_template

from flask_bootstrap import Bootstrap

from flask_login import LoginManager

from flask_migrate import Migrate

from flask_sqlalchemy import SQLAlchemy


# database variable initialization
db = SQLAlchemy()
login_manager = LoginManager()


# Initialize the App
def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    Bootstrap(app)
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"

    # TODO: Check if is really needed to use migrations each time the app runs.
    migrate = Migrate(app, db)

    from app import models

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint)

    # TODO:
    #   - html templates for errors handlers only differ in the hard coded text,
    #      thus we may use a single template with variables to inject the text.
    #   - understand why each error handler as a parameter error not being used?
    #   - can we extract from the `error` parameter a message to display?

    @app.errorhandler(403)
    def forbidden(error):
        return render_template(
            'errors/403.html',
            title='Forbidden'
        ), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template(
            'errors/404.html',
            title='Server Error'
        ), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template(
            'errors/500.html',
            title="Server Error"
        )


    return app
