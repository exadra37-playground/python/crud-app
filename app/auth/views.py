from flask import flash, redirect, render_template, url_for

from flask_login import login_required, login_user, logout_user

from forms import LoginForm, RegistrationForm

from . import auth

from .. import db

from ..models import Employee


# TODO: see if is possible to handler GET and POST routes in separated classes
@auth.route('/register', methods=['GET', 'POST'])
def register():
    """
    Handle requests to the /register route
    Add an employee to the database through the registration form
    """

    form = RegistrationForm()

    if form.validate_on_submit():

        employee = Employee(
            email=form.email.data,
            username=form.username.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            password=form.password.data
        )

        # Add employee to database
        #
        # TODO:
        #    - try to use Repository to save employee into database
        #    - understand the use of session to add a user into Employees table
        db.session.add(employee)
        db.session.commit()
        flash('You have successfully registered! You may now login')

        return redirect(url_for('auth.login'))

    # Load the registration template
    return render_template('auth/register.html', form=form, title='Register')


# TODO: try to use a different file for Login route
@auth.route('/login', methods=['GET', 'POST'])
def login():
    """
    Handle requests for the /login route.
    Log an employee in through the login form
    """

    form = LoginForm()

    if form.validate_on_submit():

        # check wether employee exists in the database and whether the password
        #  entered matches the password in the database.
        employee = Employee.query.filter_by(email=form.email.data).first()

        if employee is not None and employee.verify_password(
            form.password.data
        ):

            login_user(employee)

            if employee.is_admin:
                return redirect(url_for('home.admin_dashboard'))
            else:
                return redirect(url_for('home.dashboard'))

            return redirect(url_for('home.dashboard'))

        else:
            flash('Invalid email or password.')

    return render_template('auth/login.html', form=form, title='Login')


# TODO: try to move to is own file
@auth.route('/logout')
@login_required
def logout():
    """
    Handle requests to the /logout route
    Log an employee out through the logout link
    """

    logout_user()
    flash('You have successfully been logged out.')

    return redirect(url_for('auth.login'))
