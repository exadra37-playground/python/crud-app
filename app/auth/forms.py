from flask_wtf import FlaskForm

from wtforms import PasswordField, StringField, SubmitField, ValidationError

from wtforms.validators import DataRequired, Email, EqualTo

from ..models import Employee


class RegistrationForm(FlaskForm):
    """
    From for users to create new account
    """

    email = StringField('Email', validators=[DataRequired(), Email()])
    username = StringField('Username', validators=[DataRequired()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    password = PasswordField('Password', validators=[
        DataRequired(),
        EqualTo('confirm_password')
    ])
    confirm_password = PasswordField('Confirm Password')
    submit = SubmitField('Register')

    # TODO:
    #  - learn why self is used so many times as first argument
    #  - try validation logic in a separated class
    def validate_email(self, field):
        # TODO: check if this form class already validate and sanitize email
        #        before we try to use it in the query.
        if Employee.query.filter_by(email=field.data).first():
            raise ValidationError('Email is already in use.')

    # TODO: - try validation logic in a separated class.
    def validate_username(self, field):
        if Employee.query.filter_by(username=field.data).first():
            raise ValidationError('Username is already in use.')


# TODO: try this class in a separate file
class LoginForm(FlaskForm):
    """
    Form for users to login
    """

    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
